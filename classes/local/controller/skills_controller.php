<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Infos controller.
 *
 * @package    block_xp
 * @copyright  2017 Frédéric Massart
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_xp\local\controller;
defined('MOODLE_INTERNAL') || die();

include_once(dirname(__FILE__) . "/../../form/skills_form.php");

use block_xp\form\skills_form;

/**
 * Skills controller class.
 *
 * @package    block_xp
 * @copyright  2017 Frédéric Massart
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class skills_controller extends page_controller {

    protected $requiremanage = false;
    protected $requireview = true;
    protected $routename = 'skills';

    private $order = array('red', 'grey', 'blue', 'orange', 'yellow', 'green');

    /** @var moodleform The form. */
    protected $form;


    protected function get_page_html_head_title() {
        return get_string('infos', 'block_xp');
    }

    protected function get_page_heading() {
        return get_string('infos', 'block_xp');
    }

    protected function get_form() {
        if (!$this->form) {
            $this->form = new skills_form($this->pageurl->out(false), [
                'config' => $this->world->get_config()
            ]);
        }
        return $this->form;
    }

    protected function pre_content() {
        $form = $this->get_form();
        $form->set_data(array("group" => $groupid));

        if ($form->is_cancelled()) {
            $this->redirect($this->urlresolver->reverse('infos', ['courseid' => $this->courseid]));
        }
    }

    protected function page_content() {
        $form = $this->get_form();
        if ($form->is_submitted() && !$form->is_validated() && !$form->no_submit_button_pressed()) {
            echo $this->get_renderer()->notification(get_string('errorformvalues', 'block_xp'));
        }
        $data = $form->get_data();
        $form->display();
        echo '<style>.node circle{fill:#fff;stroke:#4682b4;stroke-width:.3px}.node{font:10px sans-serif}.link{fill:none;stroke:grey;stroke-width:2.5px}.mouse{cursor:pointer}
        </style>
        <script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
        <script>var json = ' . json_encode($this->tree($data->group)) . ';function drawSVG(t){if(""!==t.name){var n=800,e=600,r=d3.layout.tree().size([e,n-300]),a=d3.svg.diagonal().projection(function(t){return[t.y,t.x]}),l=d3.select("#box").append("svg").attr("width",n).attr("height",e).append("g").attr("class","mouse").attr("transform","translate(140,0)"),i=r.nodes(t),o=r.links(i),d=(l.selectAll("path.link").data(o).enter().append("path").attr("class","link").style("stroke",function(t){return t.target.path}).attr("d",a),l.selectAll("g.node").data(i).enter().append("g").attr("class","node").attr("transform",function(t){return"translate("+t.y+","+t.x+")"}));d.append("circle").attr("r",6.5).style("fill",function(t){return t.color}).on("click",function(t){null!==t.link&&void 0!==t.link&&window.open(t.link,"_self")}),d.append("text").attr("dx",function(t){return t.children?-8:8}).attr("dy",3).attr("text-anchor",function(t){return t.children?"end":"start"}).text(function(t){return t.name}).on("click",function(t){null!==t.link&&void 0!==t.link&&window.open(t.link,"_self")}),d3.select(self.frameElement).style("height",e+"px")}}drawSVG(json);
        </script>';
    }

    function toDays($secs) {
        return $secs / 60 / 60 / 24;
    }
    
    function tree($groupid) {
        global $DB;
        $skill = $DB->get_record('skills', array('id'=> $groupid));
        $el = array();
        $el['name'] = str_replace("_", " ", $skill->name);
        $el['color'] = 'red';
        $el['path'] = 'grey';
        $el['userid'] = 2;
        //$el['link'] = 'editgroup.php?id='.$skill->id;
        
        $skillsgroup = $DB->get_records('skills_group', array('groupid'=>$groupid));
        
        foreach ($skillsgroup as $groupdata) {
            $skillsdata = $DB->get_record('skills', array('id'=>$groupdata->skillsid));	
            $el['children'][] = $this->tree_element($skillsdata, $groupdata->bedingung, $skill->isgroup);
        
        }
        $i = 0;
        foreach ($el['children'] as $child) {
            if ($i == 0 && $child["path"] == "green") {
                $el['color'] = "green";
            }
        
            if($child["path"] == "green" && array_search($child['color'], $this->order) < array_search($el['color'], $this->order)) {
                $el['color'] = $child['color'];
            } else if ($child["path"] == "blue" && array_search($child['color'], $this->order) > array_search($el['color'], $this->order)) {
                $el['color'] = $child['color'];				
            }
            $i++;
        }
        return $el;
    }
    
    
    function tree_element($skillsdata, $bedingung, $isgroup) {
        global $CFG, $DB, $USER;
        
        $shortname = str_replace("_", " ", $skillsdata->name);
    
        $elements['children'] = array();
        
        if($isgroup == 1) {
            $path = 'green';
            $color = 'green';
        } else {
            $color = 'grey';
            $path = 'blue';
        }
        
        $skillsissues = array_values($DB->get_records('skills_issues', array('userid'=> $USER->id, 'skillid'=>$skillsdata->id)))[0];
        $zertifikat = $DB->get_record('zertifikat', array('skillid' => $skillsdata->id));
        $zertifkatissue = $DB->get_record('zertifikat_issues', array('userid' => $USER->id, 'zertifikatid' => $zertifikat->id));
        $tmp = $skillsissues;
        
        $expire = $this->toDays($tmp->timeend - time());
        
        if(!isset($tmp->timeend)) {
            $color = 'grey';
        } else if($expire <= 0) {
            $color = 'red';
        } else if(empty($tmp->filename) ) {
            $color = "blue";
        } else if($expire >= 90) {
            $color = 'green';
        } else if($expire < 90 && $expire >= 30) {
            $color = 'yellow';
        } else if($expire < 30) {
            $color = 'orange';
        } 
    
        if($skillsdata->isgroup)
        {
            $skillsgroupnew = $DB->get_records('skills_group', array('groupid'=>$skillsdata->id));
            $skillnew = $DB->get_record('skills', array('id'=> $skillsdata->id));
            
            foreach ($skillsgroupnew  as $groupdatanew )
            {
                $skillsdatanew  = $DB->get_record('skills', array('id'=>$groupdatanew ->skillsid));
                $isgroupnew = $skillnew->isgroup;
                $elements['children'][] = $this->tree_element($skillsdatanew, $groupdatanew ->bedingung, $isgroupnew, '');
            }
            //$link = 'editgroup.php?id='.$skillsdata->id;
            foreach ($elements['children'] as $child) {
                if($child["path"] == "green" && array_search($child['color'], $this->order) < array_search($color, $this->order)) {
                    $color = $child['color'];
                } else if ($child["path"] == "blue" && array_search($child['color'], $this->order) > array_search($color, $this->order)) {
                    $color = $child['color'];				
                }
            }
        } else {
            $enrolment = $DB->get_record('enrol', array('courseid' => $zertifikat->course));
            $user_enrolment = $DB->get_record('user_enrolments', array('enrolid' => $enrolment->id, 'userid' => $USER->id));
            if(!empty($user_enrolment)) {
                $link = $CFG->wwwroot . '/course/view.php?id='.$zertifikat->course;			
            } else {
                $link = "mailto:it-dwtoc@deutsche-windtechnik.com?subject=Moodle Anfrage Kurseinscheibung " . $shortname . "&body=Hallo IT, %0A%0Ahiermit beantrage ich, ". $USER->firstname . " " . $USER->lastname . " (" . $USER->email . "), die Einschreibung in folgenden E-Learning Kurs: ". $shortname . "%0A%0ABitte Rücksprache mit dem Dispatch und dem Teilnehmer halten.%0A%0ADiese Email wurde auf Anfrage des Nutzers automatisch vom Moodle-System generiert.";
            }
    
        }
    
        $element = array('skillid'=>$skillsdata->id, 'group'=>$skillsdata->isgroup, 'name'=>$shortname, 'children'=>$elements['children'], 'color' => $color, 'path' => $path, 'link' => $link);
            
        return $element;
    } 

}
