<?php 

global $CFG;

require_once("$CFG->libdir/formslib.php");

$order = array('red', 'grey', 'blue', 'orange', 'yellow', 'green');

class skill_form extends moodleform {
   private $groupid;
   //Add elements to form
   public function definition() {
	   global $DB;

	   $mform = $this->_form; // Don't forget the underscore! 
	
	   $grouplist = array();
	   $select = "visible = '1' AND isgroup != '0'";
	   $groups = $DB->get_records_select('skills', $select, array(), 'shortname');
	   foreach ($groups as $value)
	   {
		   $grouplist[$value->id] = $value->name;
	   }

	   $mform->addElement('select', 'group', "Gruppe wählen", $grouplist); // Add elements to your form
	   $buttonarray[] = $mform->createElement('submit', 'submitbutton', get_string('show'));
	   $mform->addGroup($buttonarray, 'buttonar', '', ' ', false);
   }

   //Custom validation should be added here
   function validation($data, $files) {

	   return array();
   }
}

function toDays($secs) {
	return $secs / 60 / 60 / 24;
}

function tree($groupid) {
	global $DB, $order;
	$skill = $DB->get_record('skills', array('id'=> $groupid));
	$el = array();
	$el['name'] = str_replace("_", " ", $skill->name);
	$el['color'] = 'red';
	$el['path'] = 'grey';
	$el['userid'] = 2;
	//$el['link'] = 'editgroup.php?id='.$skill->id;
	
	$skillsgroup = $DB->get_records('skills_group', array('groupid'=>$groupid));
	
	foreach ($skillsgroup as $groupdata) {
		$skillsdata = $DB->get_record('skills', array('id'=>$groupdata->skillsid));	
		$el['children'][] = tree_element($skillsdata, $groupdata->bedingung, $skill->isgroup);
	
	}
	$i = 0;
	foreach ($el['children'] as $child) {
		if ($i == 0 && $child["path"] == "green") {
			$el['color'] = "green";
		}
	
		if($child["path"] == "green" && array_search($child['color'], $order) < array_search($el['color'], $order)) {
			$el['color'] = $child['color'];
		} else if ($child["path"] == "blue" && array_search($child['color'], $order) > array_search($el['color'], $order)) {
			$el['color'] = $child['color'];				
		}
		$i++;
	}
	return $el;
}


function tree_element($skillsdata, $bedingung, $isgroup) {
	global $CFG, $DB, $USER, $order;
	
	$shortname = str_replace("_", " ", $skillsdata->name);

	$elements['children'] = array();
	
	if($isgroup == 1) {
		$path = 'green';
		$color = 'green';
	} else {
		$color = 'grey';
		$path = 'blue';
	}
	
	$skillsissues = array_values($DB->get_records('skills_issues', array('userid'=> $USER->id, 'skillid'=>$skillsdata->id)))[0];
	$zertifikat = $DB->get_record('zertifikat', array('skillid' => $skillsdata->id));
	$zertifkatissue = $DB->get_record('zertifikat_issues', array('userid' => $USER->id, 'zertifikatid' => $zertifikat->id));
	$tmp = $skillsissues;
	
	$expire = toDays($tmp->timeend - time());
	
	if(!isset($tmp->timeend)) {
		$color = 'grey';
	} else if($expire <= 0) {
		$color = 'red';
	} else if(empty($tmp->filename) ) {
		$color = "blue";
	} else if($expire >= 90) {
		$color = 'green';
	} else if($expire < 90 && $expire >= 30) {
		$color = 'yellow';
	} else if($expire < 30) {
		$color = 'orange';
	} 

	if($skillsdata->isgroup)
	{
		$skillsgroupnew = $DB->get_records('skills_group', array('groupid'=>$skillsdata->id));
		$skillnew = $DB->get_record('skills', array('id'=> $skillsdata->id));
		
		foreach ($skillsgroupnew  as $groupdatanew )
		{
			$skillsdatanew  = $DB->get_record('skills', array('id'=>$groupdatanew ->skillsid));
			$isgroupnew = $skillnew->isgroup;
			$elements['children'][] = tree_element($skillsdatanew, $groupdatanew ->bedingung, $isgroupnew, '');
		}
		//$link = 'editgroup.php?id='.$skillsdata->id;
		foreach ($elements['children'] as $child) {
			if($child["path"] == "green" && array_search($child['color'], $order) < array_search($color, $order)) {
				$color = $child['color'];
			} else if ($child["path"] == "blue" && array_search($child['color'], $order) > array_search($color, $order)) {
				$color = $child['color'];				
			}
		}
	} else {
		$enrolment = $DB->get_record('enrol', array('courseid' => $zertifikat->course));
		$user_enrolment = $DB->get_record('user_enrolments', array('enrolid' => $enrolment->id, 'userid' => $USER->id));
		if(!empty($user_enrolment)) {
			$link = $CFG->wwwroot . '/course/view.php?id='.$zertifikat->course;			
		} else {
			$link = "mailto:it-dwtoc@deutsche-windtechnik.com?subject=Moodle Anfrage Kurseinscheibung " . $shortname . "&body=Hallo IT, %0A%0Ahiermit beantrage ich, ". $USER->firstname . " " . $USER->lastname . " (" . $USER->email . "), die Einschreibung in folgenden E-Learning Kurs: ". $shortname . "%0A%0ABitte Rücksprache mit dem Dispatch und dem Teilnehmer halten.%0A%0ADiese Email wurde auf Anfrage des Nutzers automatisch vom Moodle-System generiert.";
		}

	}

	$element = array('skillid'=>$skillsdata->id, 'group'=>$skillsdata->isgroup, 'name'=>$shortname, 'children'=>$elements['children'], 'color' => $color, 'path' => $path, 'link' => $link);
		
	return $element;
} 