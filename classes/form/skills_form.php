<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block XP levels form.
 *
 * @package    block_xp
 * @copyright  2014 Frédéric Massart
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_xp\form;

defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir . '/formslib.php');

use moodleform;

/**
 * Block XP levels form class.
 *
 * @package    block_xp
 * @copyright  2014 Frédéric Massart
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class skills_form extends moodleform {

    /** @var config The config. */
    protected $config;

    /**
     * Form definintion.
     *
     * @return void
     */
    public function definition() {
        global $DB;
        
        $mform = $this->_form; // Don't forget the underscore! 
            
        $grouplist = array();
        $select = "visible = '1' AND isgroup != '0'";
        $groups = $DB->get_records_select('skills', $select, array(), 'shortname');
        foreach ($groups as $value) {
            $grouplist[$value->id] = $value->name;
        }
    
        $mform->addElement('select', 'group', "Gruppe wählen", $grouplist); // Add elements to your form
        $buttonarray[] = $mform->createElement('submit', 'submitbutton', get_string('show'));
        $buttonarray[] = $mform->createElement('cancel');        
        $mform->addGroup($buttonarray, 'buttonar', '', ' ', false);

        $mform->addElement('html', '<div id="box">');
    }

    /**
     * Data validate.
     *
     * @param array $data The data submitted.
     * @param array $files The files submitted.
     * @return array of errors.
     */
    public function validation($data, $files) {
        return array();
    }
}
